#include <iostream>
#include <cstdio>
#include <vector>
#include <utility>
#include <deque>
#include <cmath>

//#define DEBUG
//#define DEBUG_ELEM_AT
//#define DEBUG_REMOVE

#define numeric int
#define getchar_custom getc_unlocked
#define EOF_CHAR (-49)

/////////////////////////////////////////////////////////////////////////////////////////
// Structs
/////////////////////////////////////////////////////////////////////////////////////////

struct TreeNode {
    numeric value;
    TreeNode* l;
    TreeNode* p;
    TreeNode* r;
    TreeNode* lBro;
    TreeNode* rBro;
};

/////////////////////////////////////////////////////////////////////////////////////////
// Reading chars
/////////////////////////////////////////////////////////////////////////////////////////

inline int read_custom() {

    register numeric tempReadingChar = getchar_custom(stdin) - 48;

    while (tempReadingChar < 0 || tempReadingChar > 9) {
		if (tempReadingChar == EOF_CHAR) {
			return -1;
		}

        tempReadingChar = getchar_custom(stdin) - 48;
    }   

    register int returnValue = 0;

    while (tempReadingChar >= 0 && tempReadingChar <= 9) {
        returnValue = (returnValue << 3) + (returnValue << 1) + tempReadingChar;
        tempReadingChar = getchar_custom(stdin) - 48;
    }

    return returnValue;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Interval Tree
/////////////////////////////////////////////////////////////////////////////////////////

class IntervalTree {

    public:
        IntervalTree(std::deque<TreeNode*> &leaves);

        numeric size();
        void insertAfterLeaf(TreeNode* leaf, numeric value);
        void printLeavesFromIndex(numeric index);
        void remove(TreeNode* node);
        TreeNode* leafAt(numeric index);

    private:
        TreeNode* head;
        numeric realSize;

        bool isLeaf(TreeNode* node);
        void recalculateSum(TreeNode* leaf);
};

inline bool IntervalTree::isLeaf(TreeNode* node) {
    return node->l == NULL && node->r == NULL;
}

void IntervalTree::printLeavesFromIndex(numeric index) {

    TreeNode* leaf = this->leafAt(index);
    TreeNode* h = leaf;
    while (leaf->lBro != h) {
        std::cout << leaf->value << " ";
        leaf = leaf->lBro;
    }

    std::cout << leaf->value;
}

inline numeric IntervalTree::size() {
    return this->realSize;
}

void IntervalTree::remove(TreeNode* node){

    if (node->p == NULL){
        this->realSize = 0;
        this->head = NULL;
        delete node;
        return;
    }

    TreeNode* p = node->p;
    
    if (p->l == node) {
        p -> l = NULL;
    } else {
        p -> r = NULL;
    }

    node->rBro->lBro = node->lBro;
    node->lBro->rBro = node->rBro;

    delete node;
    --this->realSize;

    if (this->isLeaf(p)) {
        TreeNode* pp = p->p;

        while (pp != NULL){
            if (pp->l == p){
                pp->l = NULL;
            } else{
                pp->r = NULL;
            }

            if (!this->isLeaf(pp)) {
                if (pp->l != NULL) {
                    this->recalculateSum(pp->l);
                } else {
                    this->recalculateSum(pp->r);
                }
				return;
            } else {
                p = pp;
                pp = pp->p;
            }
        }

        return;
    }

    if (p->l != NULL) {
        this->recalculateSum(p->l);
    } else {
        this->recalculateSum(p->r);
    }
}

TreeNode* IntervalTree::leafAt(numeric index) {
    TreeNode* travel = this->head;

    while (!this->isLeaf(travel)) {

        if (travel->l != NULL
                && travel->r == NULL) {
            travel = travel->l;
            continue;
        }

        if (travel->l == NULL
                && travel->r != NULL) {
            travel = travel->r;
            continue;
        }

        TreeNode* l = travel->l;
        TreeNode* r = travel->r;

        if (!this->isLeaf(l)) {
			if (!this->isLeaf(r))
			{
				if (r->value > index) {
					travel = r;
				} else {
					travel = l;
					numeric prevIndex = index;
					index -= r->value;
				}
			} else {
				if (index == 0) {
					travel = r;
				} else {
					travel = l;
					--index;
				}
			}
        } else {	
			if (!this->isLeaf(r))
			{
				if (index >= r->value) {
				   travel = l;
				   index = 0;
			   } else {
				   travel = r;
			   }
			} else {
			   if (index == 0) {
				   travel = r;
			   } else {
				   travel = l;
				   index = 0;
			   }
			}
	   }
    }
    return travel;
}

void IntervalTree::insertAfterLeaf(TreeNode* leaf, numeric value) {

    if (leaf->p != NULL) {

        TreeNode* leafParent = leaf->p;

        TreeNode* valNode = new TreeNode;
        valNode->value = value;
        valNode->l = NULL;
        valNode->r = NULL;
        valNode->p = NULL;

        TreeNode* connector = new TreeNode;
        connector->r = leaf;
        connector->l = valNode;
        connector->p = leafParent;
        connector->value = 2;
        leaf->p = connector;
        valNode->p = connector;

        if (leafParent->r == leaf) {
            leafParent->r = connector;
        } else {
            leafParent->l = connector; 
        }

        TreeNode* oldBro = leaf->lBro;
        leaf->lBro = valNode;
        valNode->rBro = leaf;
        valNode->lBro = oldBro;
        oldBro->rBro = valNode;

    } else {
        TreeNode* valNode = new TreeNode;
        valNode->value = value;
        valNode->l = NULL;
        valNode->r = NULL;
        valNode->p = NULL;

        TreeNode* connector = new TreeNode;
        connector->r = leaf;
        connector->l = valNode;
        connector->p = NULL;
        connector->value = 2;
        leaf->p = connector;
        valNode->p = connector;
        this->head = connector;
        
        TreeNode* oldBro = leaf->lBro;
        leaf->lBro = valNode;
        valNode->rBro = leaf;
        valNode->lBro = oldBro;
        oldBro->rBro = valNode;
    }

    this->realSize += 1;

    this->recalculateSum(leaf);
}

void IntervalTree::recalculateSum(TreeNode* leaf) {
    leaf = leaf->p;

    while (leaf != NULL) {

        TreeNode* r = leaf->r;
        TreeNode* l = leaf->l;

        if (l == NULL) {

            if (this->isLeaf(r)) {
                leaf -> value = 1;
            } else {
                leaf -> value = r->value;
            }

            leaf = leaf->p;
            continue;
        }

        if (r == NULL) {
            if (this->isLeaf(l)) {
                leaf -> value = 1;
            } else {
                leaf -> value = l->value;
            }
    
            leaf = leaf->p;
            continue;
        }

        if (this->isLeaf(r)) {
            
            if (this->isLeaf(l)) {
                leaf -> value = 2;
                leaf = leaf->p;
            } else {
                leaf -> value = 1 + l-> value;
                leaf = leaf->p;
            }
        } else {
			if (this->isLeaf(l)) {
				leaf -> value = 1 + r->value;
				leaf = leaf->p;
			} else {
				leaf -> value = r->value + l->value;
				leaf = leaf->p;
			}
		}
    }

}

IntervalTree::IntervalTree(std::deque<TreeNode*> &leaves) {

    int initSize = leaves.size();
    this->realSize = initSize;

    numeric powLocal = ceil(log2(initSize));
    numeric totalLeaves = pow(2, powLocal);

    for (numeric i = initSize; i < totalLeaves; i++) {

        TreeNode* node = new TreeNode;
        node->l = NULL;
        node->r = NULL;
        node->p = NULL;
        node->lBro = NULL;
        node->rBro = NULL;
        node->value = 0;

        leaves.push_back(node);

    }

    std::deque<TreeNode*> innerNodes;

    while (leaves.size() > 0) {
        TreeNode* firstNode = leaves.front();
        leaves.pop_front();

        TreeNode* secondNode = NULL;
        secondNode = leaves.front();
        leaves.pop_front();

        TreeNode* newNode = new TreeNode;
        newNode->r = firstNode;
        newNode->l = secondNode;
        newNode->p = NULL;
        newNode->lBro = NULL;
        newNode->rBro = NULL;
        firstNode->p = newNode;

        numeric sum = 0;
        secondNode->p = newNode;
        sum = 2;

        newNode->value = sum;
        innerNodes.push_back(newNode);
    }

    while (innerNodes.size() > 1) {

        TreeNode* stNode = innerNodes.front();
        innerNodes.pop_front();
        
        TreeNode* ndNode = innerNodes.front();
        innerNodes.pop_front();


        TreeNode* node = new TreeNode;
        node->l = ndNode;
        node->r = stNode;
        node->p = NULL;
        node->value = stNode->value + ndNode->value;

        stNode->p = node;
        ndNode->p = node;

        innerNodes.push_back(node);

    }

    head = innerNodes.front();

}

/////////////////////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////////////////////

int main() {

    numeric repeats;
    repeats = read_custom();

    // fill list
    numeric readValue = read_custom();

    std::deque<TreeNode*> leaves;

    while (readValue >= 0) {

        TreeNode* node = new TreeNode;
        node->l = NULL;
        node->r = NULL;
        node->p = NULL;
        node->lBro = NULL;
        node->rBro = NULL;
        node->value = readValue;

        if (leaves.size() > 0) {
            TreeNode* previous = leaves.back();
            previous->lBro = node;
            node->rBro = previous;   
        }

        leaves.push_back(node);

        readValue = read_custom();
    }

    if (leaves.size() > 0) {
        TreeNode* node = leaves.back();
        TreeNode* first = leaves.front();
        first->rBro = node;
        node->lBro = first;

    }

    IntervalTree tree = IntervalTree(leaves);
    
    register numeric POS = 0;

    for (numeric i = 0; i < repeats; i++){
        TreeNode* node = tree.leafAt(POS);
        numeric value = node->value;

        if (!(value & 1)) {
           
           numeric nextIndex = (POS + 1) % tree.size(); 
           numeric nextValue = node->lBro->value;

           tree.remove(node->lBro);

           if (nextIndex < POS) {
               --POS;
           }

           POS = (POS + nextValue) % tree.size();         
        } else {        
            numeric nextPos = (POS + 1) % tree.size();
            tree.insertAfterLeaf(node, value - 1);
           
            POS = (POS + value) % tree.size();

        }
    }
    tree.printLeavesFromIndex(POS);
}
